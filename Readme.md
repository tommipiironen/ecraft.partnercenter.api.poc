# Microsoft Partners Center API POC

This PoC demostrates how to use Partners Center Api.

###### App credentials:
DevEspoo TeamCreds-keepass, under eCraft/PartnerCenter/Partner center api PROD

###### Api documentation: 
https://docs.microsoft.com/en-us/partner-center/develop/scenarios

###### Partners Center dashboard Url:
https://partner.microsoft.com/en-us/dashboard/home

