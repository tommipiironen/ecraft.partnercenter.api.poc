﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace PartnerCenterClient
{
    class Program
    {
        /*
         This is a POC to show how to use MS Partners api to fetch customer's invoice lines.
         This example queries Almaco's "office"-related invoice lines, ie Dynamics products.
         */

        static async Task Main(string[] args)
        {
            await QueryInvoiceLines();
        }

        static async Task QueryInvoiceLines()
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", true, true)
                .Build();

            //var resource = "https://graph.windows.net";
            var resource = "https://graph.windows.net";
            var authContext = new AuthenticationContext(config["AADInstance"]);
            var clientId = config["ClientId"];
            var clientSecret = config["ClientSecret"];
            var clientCred = new ClientCredential(clientId, clientSecret);
            var result = await authContext.AcquireTokenAsync(resource, clientCred);

            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + result.AccessToken);
            client.DefaultRequestHeaders.Add("ms-requestid", Guid.NewGuid().ToString());
            client.DefaultRequestHeaders.Add("ms-correlationid", Guid.NewGuid().ToString());
            client.DefaultRequestHeaders.Add("Accept", "application/json");

            var tenantId = "3e366e51-80d1-4ffc-bacf-15627233f051";
            var url = $"https://api.partnercenter.microsoft.com/v1/customers/{tenantId}/subscriptions";
            var req = new HttpRequestMessage(HttpMethod.Get, url);
            var res = await client.SendAsync(req);
            var parsed = await res.Content.ReadAsStringAsync();

            await GetPdf(client);
            // Get list of invoices.
            // Documentation at: https://docs.microsoft.com/en-us/partner-center/develop/get-a-collection-of-invoices#rest-request
            var invoiceListReq = new HttpRequestMessage(HttpMethod.Get, "https://api.partnercenter.microsoft.com/v1/invoices");

            var invoicelistRes = await client.SendAsync(invoiceListReq);
            var resBody = await invoicelistRes.Content.ReadAsStringAsync();

            var json = JObject.Parse(resBody);
            // parse list of invoice headers
            var items = json["items"].Children();

            // id of first invoice. Invoices are sorted descending by invoice date
            var id = (string)items.First()["id"];

            // Get the invoice
            //  provider=office -parameter returns Dynamics-products (in almaco's case). If you want to get azure-related produts use provider=azure
            // Docs: https://docs.microsoft.com/en-us/partner-center/develop/get-invoice-by-id#rest-request
            var req2 = new HttpRequestMessage(HttpMethod.Get, $"https://api.partnercenter.microsoft.com/v1/invoices/{id}/lineitems?provider=office&invoicelineitemtype=billinglineitems");

            var response = await client.SendAsync(req2);
            resBody = await response.Content.ReadAsStringAsync();

            json = JObject.Parse(resBody);

            var lineItems = json["items"].Children();
            // Get Almaco's invoice lines only
            var almacoItems = lineItems.Where(i => string.Equals((string)i["customerName"], "Almaco", StringComparison.OrdinalIgnoreCase)).ToList();

            almacoItems.ForEach(i => Console.WriteLine((string)i["offerName"]));
        }

        public static async Task GetPdf(HttpClient client)
        {

            var invoiceListReq = new HttpRequestMessage(HttpMethod.Get, "https://api.partnercenter.microsoft.com/v1/invoices/D04000237V/documents/statement");

            var invoicelistRes = await client.SendAsync(invoiceListReq);
            var bytes = await invoicelistRes.Content.ReadAsByteArrayAsync();
            File.WriteAllBytes("ouput.pdf", bytes);
        }
    }
}
